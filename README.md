# Requirements

1. You will need to have used ECS before so that the ecsTaskExecutionRole and AWSServiceRoleForECS roles are created.
2. You will need to manually provision an SSL Certificate in the AWS console for the region that you are going to be working in.


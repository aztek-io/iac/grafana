########################################
### Variables ##########################
########################################

data "aws_availability_zones" "available" {}

variable "global" {
    type = "map"
    default = {
        environment = "Development"
        region      = "us-west-2"
        profile     = "default"
    }
}

variable "vpc" {
    type = "map"
    default = {
        vpc_name            = "Grafana"
        vpc_cidr            = "100.64.0.0/16"

        fargate_subnet_name = "Grafana Fargate"

        grafana_subnet_1    = "100.64.101.0/24"
        grafana_subnet_2    = "100.64.102.0/24"
        grafana_subnet_3    = "100.64.103.0/24"

        rds_subnet_name     = "Grafana RDS"

        rds_subnet_1        = "100.64.201.0/24"
        rds_subnet_2        = "100.64.202.0/24"
        rds_subnet_3        = "100.64.203.0/24"
    }
}

variable "lb" {
    type = "map"
    default = {
        name        = "grafana-alb"
        internal    = false
        lb_type     = "application"

        tg_name     = "grafana-tg"
        tg_port     = 80
        tg_protocol = "HTTP"
        tg_type     = "ip"

        hc_path     = "/login"
        hc_port     = 3000
        hc_matcher  = "200-299"

        list_port   = 80
        list_proto  = "HTTP"

        list_port2  = 443
        list_proto2 = "HTTPS"
    }
}

variable "ecs" {
    type = "map"
    default = {
        cluster_name        = "Grafana"
        task_cpu            = 512
        task_memory         = 1024
        container_port      = 3000
        container_image     = "grafana/grafana:5.2.2"
        service_name        = "grafana"
        desired_count       = 1
        lb_container_name   = "grafana"
        hc_grace_period     = 10
        policy              = <<ECS_POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
ECS_POLICY
    }
}

variable "rds" {
    type = "map"
    default = {
        db_name                 = "grafana"
        db_user                 = "admin"
        db_password             = "password1"
        db_port                 = 3306
        db_cluster              = "grafana"
        db_type                 = "default.aurora5.6"
        db_backup_retention     = 7
        db_backup_window        = "18:30-19:00"
        db_maint_window         = "mon:06:45-mon:07:15"
        db_encryption           = true
        db_final_snap           = "grafana"
        db_count                = 1
        db_identifier           = "grafana"
        db_identifier_cluster   = "grafana"
        db_instance_type        = "db.t2.small"
    }
}

variable "route53" {
    type = "map"
    default = {
        subdomain   = "grafana"
        domain      = "scriptmyjob.com"
    }
}

provider "aws" {
    # access_key  = ""
    # secret_key  = ""
    region      = "${lookup(var.global,"region")}"
    profile     = "${lookup(var.global,"profile")}"
}

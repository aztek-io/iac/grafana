resource "aws_s3_bucket" "grafana" {
    bucket  = "grafana-images.${lookup(var.route53,"domain")}"
    acl     = "public-read"
}

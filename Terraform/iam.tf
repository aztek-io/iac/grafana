########################################
### Data Sources #######################
########################################

data "aws_iam_role" "ecsTaskExecutionRole" {
    name = "ecsTaskExecutionRole"
}

data "aws_iam_role" "AWSServiceRoleForECS" {
    name = "AWSServiceRoleForECS"
}

########################################
### IAM Policy Documents ###############
########################################

data "aws_iam_policy_document" "grafana" {
    statement {
        effect = "Allow"
        actions = [
            "s3:*"
        ]
        resources = [
            "arn:aws:s3:::${aws_s3_bucket.grafana.id}/*"
        ]
    }
    statement {
        effect = "Allow"
        actions = [
            "s3:*"
        ]
        resources = [
            "arn:aws:s3:::${aws_s3_bucket.grafana.id}"
        ]
    }
}

########################################
### IAM Policies #######################
########################################

resource "aws_iam_policy" "grafana" {
    name    = "Grafana"
    policy  = "${data.aws_iam_policy_document.grafana.json}"
}

########################################
### IAM Roles ##########################
########################################

resource "aws_iam_role" "grafana" {
    name        = "Grafana"
    description = "This is a role to allow Fargate to put objects in s3."
    assume_role_policy = "${lookup(var.ecs,"policy")}"
}

########################################
### IAM Policy Attachments #############
########################################

resource "aws_iam_policy_attachment" "grafana" {
    name            = "Grafana"
    roles           = [
        "${aws_iam_role.grafana.name}",
    ]
    policy_arn      = "${aws_iam_policy.grafana.arn}"
}

########################################
### IAM User Configurations ############
########################################

resource "aws_iam_user" "grafana" {
    name    = "grafana-images"
}

resource "aws_iam_access_key" "grafana" {
    user    = "${aws_iam_user.grafana.name}"
}

########################################
### IAM Group Configurations ###########
########################################

resource "aws_iam_group" "grafana" {
    name    = "grafana"
}

resource "aws_iam_group_membership" "grafana" {
    group   = "${aws_iam_group.grafana.name}"
    name    = "grafana-members"
    users   = [
        "${aws_iam_user.grafana.name}"
    ]

}

resource "aws_iam_group_policy_attachment" "grafana" {
    group      = "${aws_iam_group.grafana.name}"
    policy_arn = "${aws_iam_policy.grafana.arn}"
}

resource "aws_lb" "grafana" {
    name               = "${lookup(var.lb,"name")}"
    internal           = "${lookup(var.lb,"internal")}"
    load_balancer_type = "${lookup(var.lb,"lb_type")}"

    security_groups    = [
        "${aws_security_group.grafana.id}"
    ]

    subnets            = [
        "${aws_subnet.grafana_subnet_1.id}",
        "${aws_subnet.grafana_subnet_2.id}",
        "${aws_subnet.grafana_subnet_3.id}"
    ]
}

resource "aws_lb_target_group" "grafana" {
    depends_on = [
        "aws_lb.grafana"
    ]
    name        = "${lookup(var.lb,"tg_name")}"
    port        = "${lookup(var.lb,"tg_port")}"
    protocol    = "${lookup(var.lb,"tg_protocol")}"
    target_type = "${lookup(var.lb,"tg_type")}"
    vpc_id      = "${aws_vpc.grafana.id}"

    health_check {
        path    = "${lookup(var.lb,"hc_path")}"
        port    = "${lookup(var.lb,"hc_port")}"
        matcher = "${lookup(var.lb,"hc_matcher")}"
    }
}

resource "aws_lb_listener" "grafanaHTTP" {
    load_balancer_arn   = "${aws_lb.grafana.arn}"
    port                = "${lookup(var.lb,"list_port")}"
    protocol            = "${lookup(var.lb,"list_proto")}"

    default_action {
        type            = "redirect"
        redirect {
            port        = "${lookup(var.lb,"list_port2")}"
            protocol    = "${lookup(var.lb,"list_proto2")}"
            status_code = "HTTP_301"
        }
    }
}

resource "aws_lb_listener" "grafanaHTTPS" {
    load_balancer_arn = "${aws_lb.grafana.arn}"
    port              = "${lookup(var.lb,"list_port2")}"
    protocol          = "${lookup(var.lb,"list_proto2")}"
    certificate_arn   = "${data.aws_acm_certificate.grafana.arn}"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"

    default_action {
        target_group_arn = "${aws_lb_target_group.grafana.arn}"
        type             = "forward"
    }
}

data "aws_acm_certificate" "grafana" {
    domain   = "${lookup(var.route53,"subdomain")}.${lookup(var.route53,"domain")}"
    statuses = ["ISSUED"]
}

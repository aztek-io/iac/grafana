########################################
### VPC Configs ########################
########################################

resource "aws_vpc" "grafana" {
    cidr_block  = "${lookup(var.vpc,"vpc_cidr")}"

    tags {
        Name    = "${lookup(var.vpc,"vpc_name")}"
    }

    enable_dns_hostnames    = true
}

########################################
### Internet Gateways ##################
########################################

resource "aws_internet_gateway" "grafana" {
    vpc_id = "${aws_vpc.grafana.id}"
    tags {
        Name = "grafana"
    }
}

########################################
### Subnet Configs #####################
########################################

resource "aws_subnet" "grafana_subnet_1" {
    vpc_id              = "${aws_vpc.grafana.id}"
    cidr_block          = "${lookup(var.vpc,"grafana_subnet_1")}"
    availability_zone   = "${data.aws_availability_zones.available.names[0]}"
    tags {
        Name = "${lookup(var.vpc,"fargate_subnet_name")} 1"
    }
}

resource "aws_subnet" "grafana_subnet_2" {
    vpc_id              = "${aws_vpc.grafana.id}"
    cidr_block          = "${lookup(var.vpc,"grafana_subnet_2")}"
    availability_zone   = "${data.aws_availability_zones.available.names[1]}"
    tags {
        Name = "${lookup(var.vpc,"fargate_subnet_name")} 2"
    }
}

resource "aws_subnet" "grafana_subnet_3" {
    vpc_id              = "${aws_vpc.grafana.id}"
    cidr_block          = "${lookup(var.vpc,"grafana_subnet_3")}"
    availability_zone   = "${data.aws_availability_zones.available.names[2]}"
    tags {
        Name = "${lookup(var.vpc,"fargate_subnet_name")} 3"
    }
}

resource "aws_subnet" "rds_subnet_1" {
    vpc_id              = "${aws_vpc.grafana.id}"
    cidr_block          = "${lookup(var.vpc,"rds_subnet_1")}"
    availability_zone   = "${data.aws_availability_zones.available.names[0]}"
    tags {
        Name = "${lookup(var.vpc,"rds_subnet_name")} 1"
    }
}

resource "aws_subnet" "rds_subnet_2" {
    vpc_id              = "${aws_vpc.grafana.id}"
    cidr_block          = "${lookup(var.vpc,"rds_subnet_2")}"
    availability_zone   = "${data.aws_availability_zones.available.names[1]}"
    tags {
        Name = "${lookup(var.vpc,"rds_subnet_name")} 2"
    }
}

resource "aws_subnet" "rds_subnet_3" {
    vpc_id              = "${aws_vpc.grafana.id}"
    cidr_block          = "${lookup(var.vpc,"rds_subnet_3")}"
    availability_zone   = "${data.aws_availability_zones.available.names[2]}"
    tags {
        Name = "${lookup(var.vpc,"rds_subnet_name")} 3"
    }
}

########################################
### Route Tables #######################
########################################

# Public
####################

resource "aws_default_route_table" "public_web_server" {
    default_route_table_id = "${aws_vpc.grafana.default_route_table_id}"
    tags {
        Name        = "Grafana Web Server Route Table"
    }

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.grafana.id}"
    }
}

########################################
### Security Groups ####################
########################################

resource "aws_security_group" "grafana" {
    vpc_id          = "${aws_vpc.grafana.id}"
    name            = "Grafana Web Server"
    tags {
        Name = "Grafana Web Server"
    }

    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
    }

    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
    }

    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 3000
        to_port     = 3000
        protocol    = "tcp"
    }

    egress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
    }
}

resource "aws_security_group" "mysql_inbound" {
    vpc_id          = "${aws_vpc.grafana.id}"
    name            = "Mysql Inbound Wordpress"
    tags {
        Name        = "Mysql Inbound Wordpress"
    }
    ingress {
        cidr_blocks = [
            "${lookup(var.vpc,"grafana_subnet_1")}",
            "${lookup(var.vpc,"grafana_subnet_2")}",
            "${lookup(var.vpc,"grafana_subnet_3")}"
        ]
        from_port   = 3306
        to_port     = 3306
        protocol    = "tcp"
    }
}


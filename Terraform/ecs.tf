resource "aws_ecs_cluster" "grafana" {
    name = "${lookup(var.ecs,"cluster_name")}"
}

resource "aws_ecs_task_definition" "grafana" {
    family = "${lookup(var.ecs,"service_name")}"

    requires_compatibilities    = ["FARGATE"]
    task_role_arn               = "${aws_iam_role.grafana.arn}"
    execution_role_arn          = "${data.aws_iam_role.ecsTaskExecutionRole.arn}"
    network_mode                = "awsvpc"
    cpu                         = "${lookup(var.ecs,"task_cpu")}"
    memory                      = "${lookup(var.ecs,"task_memory")}"
    container_definitions       = <<DEFINITION
[
    {
        "cpu": ${lookup(var.ecs,"task_cpu")},
        "essential": true,
        "portMappings": [
            {
                "hostPort": ${lookup(var.ecs,"container_port")},
                "protocol": "tcp",
                "containerPort": ${lookup(var.ecs,"container_port")}
            }
        ],
        "image": "${lookup(var.ecs,"container_image")}",
        "memory": ${lookup(var.ecs,"task_memory")},
        "memoryReservation": ${ lookup(var.ecs,"task_memory") / 2 },
        "name": "${lookup(var.ecs,"service_name")}",
        "environment": [
            {
                "name": "GF_DATABASE_HOST",
                "value": "${aws_rds_cluster_instance.grafana.endpoint}:3306"
            },
            {
                "name": "GF_DATABASE_NAME",
                "value": "${lookup(var.rds,"db_name")}"
            },
            {
                "name": "GF_DATABASE_USER",
                "value": "${lookup(var.rds,"db_user")}"
            },
            {
                "name": "GF_DATABASE_PASSWORD",
                "value": "${lookup(var.rds,"db_password")}"
            },
            {
                "name": "GF_DATABASE_SSL_MODE",
                "value": "false"
            },
            {
                "name": "GF_EXTERNAL_IMAGE_STORAGE_PROVIDER",
                "value": "s3"
            },
            {
                "name": "GF_EXTERNAL_IMAGE_STORAGE_S3_BUCKET",
                "value": "${aws_s3_bucket.grafana.id}"
            },
            {
                "name": "GF_EXTERNAL_IMAGE_STORAGE_S3_ACCESS_KEY",
                "value": "${aws_iam_access_key.grafana.id}"
            },
            {
                "name": "GF_EXTERNAL_IMAGE_STORAGE_S3_SECRET_KEY",
                "value": "${aws_iam_access_key.grafana.secret}"
            },
            {
                "name": "GF_EXTERNAL_IMAGE_STORAGE_S3_REGION",
                "value": "${aws_s3_bucket.grafana.region}"
            },
            {
                "name": "GF_SERVER_ROOT_URL",
                "value": "http://${lookup(var.route53,"subdomain")}.${lookup(var.route53,"domain")}/"
            },
            {
                "name": "GF_DATABASE_TYPE",
                "value": "mysql"
            }
        ]
    }
]
DEFINITION
}

data "aws_ecs_task_definition" "grafana" {
    depends_on      = [
        "aws_ecs_task_definition.grafana"
    ]
    task_definition = "${aws_ecs_task_definition.grafana.family}"
}

resource "aws_ecs_service" "grafana" {
    name            = "${lookup(var.ecs,"service_name")}"
    cluster         = "${aws_ecs_cluster.grafana.id}"
    task_definition = "${aws_ecs_task_definition.grafana.family}:${max("${aws_ecs_task_definition.grafana.revision}", "${data.aws_ecs_task_definition.grafana.revision}")}"
    desired_count   = "${lookup(var.ecs,"desired_count")}"
    launch_type     = "FARGATE"

    load_balancer {
        target_group_arn = "${aws_lb_target_group.grafana.arn}"
        container_name   = "${lookup(var.ecs,"service_name")}"
        container_port   = "${lookup(var.ecs,"container_port")}"
    }

    health_check_grace_period_seconds = "${lookup(var.ecs,"hc_grace_period")}"

    network_configuration {
        subnets     = [
            "${aws_subnet.grafana_subnet_1.id}",
            "${aws_subnet.grafana_subnet_2.id}",
            "${aws_subnet.grafana_subnet_3.id}"
        ]
        security_groups = [
            "${aws_security_group.grafana.id}"
        ]
        assign_public_ip = true
    }
}
